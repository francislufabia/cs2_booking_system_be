const express = require('express');
const router = express.Router();
const CourseController = require('../controllers/course');
const auth = require('../auth');

router.post('/course-exists', (req, res) => {
	CourseController.courseExists(req.body).then(result => res.send(result))
})

router.post('/addCourse', (req, res) => {
	CourseController.insert(req.body).then(result => res.send(result));
})

//=======================================//
//code below is to create an endpoint for our getAll function which will allow us to display all of the courses available for enrollment
//from controllers/routes.js
router.get('/', (req, res) => {
	CourseController.getAll().then(courses => res.send(courses));
})

//=======================================//
//Code below is from the controllers part to show individual courses
//task below is to make a route to get a single course from a database

router.get('/:id', (req,res) => {
	//lets capture the input of the user
	const courseId = req.params.id
	CourseController.get({ courseId }).then(course => res.send(course))
})
//the endpoint "/:" is just a placeholder paramater.
//courseId is from the controllers/course.js
//get is from the function made in controllers/course.js
//=======================================//

//=======================================//
//code below is from controllers course.js
//this is to disable the course inside the app. (archive)
//the user needs to be verified first if the user exists in the records and if the user has the correct access rights (isAdmin)
//dont forget to require auth module in the first few lines
router.delete('/:id', auth.verify, (req, res) => {
	const courseId = req.params.id
	CourseController.archive({ courseId }).then(result => res.send(result))
})


//line below is just the normal routing
module.exports = router;

