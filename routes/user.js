//lets declare the needed dependencies
const express = require('express');
const router = express.Router();
const UserController = require('../controllers/user.js');
const auth = require('../auth');

//[SECTION] Primary Routes (for main goals)

//line below is from controllers to check for the email if duplicates exists
router.post('/email-exists' , (req, res) => {
	UserController.emailExists(req.body).then(result => res.send(result))
}) //go to register.js in frontend after this

//register a new user
router.post('/register', (req, res) => {
	UserController.register(req.body).then(result => res.send(result));
}) //the .register in the userController can be found in controllers/user.js
//dont forget to export after (module.exports = router)
//go to entry point after this
//add /register sa address ng postman
// api/users/register (first two can be found in entry point)

//[SECTION] Secondary Routes (for stretch goals)

//go to app.js after this

//this part is to authenticate username and password. THis is from controllers user.js
router.post('/login', (req, res) => {
	UserController.login(req.body).then(result => res.send(result))
})


//lets create a route that will send a request to display the user'sinformation
//from controllers user.js
//upon getting the user, the user has to be authenticated to determine the proper authorization rights for the user
//dont forget to require the auth module in line 5
router.get('/details', auth.verify, (req, res) => {
	//line below: get the headers section of the request with the JWT (from auth.js)
	const user = auth.decode(req.headers.authorization)//in this line, we are strippin away the header section of the data to decode the token to get the payload
	UserController.get({ userId: user.id }).then(user => res.send(user))
})


//line below is for the stretch goals, enrollment
//from controllers.js
//add an object to the object inside the enrollment array
router.post('/enroll', auth.verify, (req, res) => {
//the auth.verify above will check if the user is logged and should have the correct access rights first before being allowed to enroll (you need to verify the identity of the user first)
//the auth is part of line 5, the verify is just the verify
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}//in the lines above, we are just capturing the data
	UserController.enroll(params).then(result => res.send(result))
})

module.exports = router;
