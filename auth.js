//the purpose of this module is to hold all the script regarding our app's authorization.
const jwt = require('jsonwebtoken');
const secret = 'CourseBookingSystem' //we have declared a secret phrase that will be used as a signature/access token for our project.
//why did we create a secret word/access token? to verify the requests coming from other apps.


// lets create a functin that will allow us to authorize a user using the access token
module.exports.createAccessToken = (user) => {
	//we first identify the props/keys of the user that we want to verify
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {})
} //through the use of the sign(), we are creating a synchronous signature with a default hash based message authentication code(HMAC) 

//access tokens can come in two forms:
//1. opaque string
//2. JSON webtoken

//the reason behind creating/utilizing two access tokens:
//1. to add another layer of security
//2. to verify if the access token came from the authorized/proper origin
//3. to add a random factor to generate an access token that only we know.


//the real value of the access token is hashed when generated.
//lets create a function to verify whether the access token is correct.
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization //authorization is a request header commonly used for HTTP basic authorization. It sets if the server requested authorization and the browser then prompted the user for a username or password.

	//lets create a control structure to describe/identify whether the token will be allowed to pass.
	if(typeof token !== "undefined"){
		//if there is a value obtained in the auhorization property, then the code below"
		token = token.slice(7, token.length) //the token will be a string datay type. the number 7 will be sliced off the string. when the JWT is generated, it will create an addtional 7 characters to include additional security in the access token
		return jwt.verify(token, secret, (err, data) => {
			return (err) ? res.send(err) : next()
			//next() is a middleware that will process the next function
		})
	}else{
		return res.send({ auth: "failed" });
	}
}



//we have created the following functions:
//a function to generate an access token via JWT with our secret word.
//a function to verify if the access token came from the correct origin or resource.
//now, its time to decode/decrypt the access token since it is still hashed.

module.exports.decode = (token) => {
	//lets create a control structure to determine the response if an access token is captured. 
	if(typeof token !== 'undefined'){
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			return (err) ? null : jwt.decode(token, {
				complete: true
			}).payload //payload is for options.
		})
	}else{
		return null //developer's choice to determine the response. null is when the data is properly identified but not given a value.
	}
}
//go to controller/user.js after this