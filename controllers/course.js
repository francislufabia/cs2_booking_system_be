const Course = require('../models/course');

//validation that no two courses exists
module.exports.courseExists = (params) => {
	return Course.find({ name: params.name }).then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.insert = (params) => {
	let course = new Course({
		name: params.name,
		description: params.description,
		price: params.price
	})
	return course.save().then((course, err) => {
		return (err) ? false : true
	})
}



//======================================//
//Displaying all of the courses that has an active status of "true"
module.exports.getAll = () => {
	return Course.find( {isActive: true} ).then(courses => courses)
}
//Course is from the model
//isActive from the model also
//isActive must be in curly braces because thats how you search sa mongdob 
//go to routes

//======================================//
//Displaying a single course//
module.exports.get = (params) => {
	return Course.findById(params.courseId).then(course => course)
}
//Course is from the model
//CourseId is the one used in routes
//go to routes


//======================================//
//disable created course function:
//this function will work on the concept of soft delete only.
//-> it will be removed in the courses catalogue but still be stored inside the database.
//"ARCHIVE FUNCTION"
module.exports.archive = (params) => {
	//our main task is to diable the course in the catalogue. target the course to be modified, then describe the changes in the doucment that will be done
	//you want it to give you a response to confirm if the request has been successful
	const updates = { isActive: false }
	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}
//go to routes after this