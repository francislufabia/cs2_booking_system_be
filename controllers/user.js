//lets acquire the needed dependecies.
const User = require('../models/user');
const bcrypt = require('bcrypt');
const auth = require('../auth'); //this line is to authenticate username and password
const Course = require('../models/course');

//create a validation to make sure that no similar email address will be included
module.exports.emailExists = (params) => {
	//upon getting the input data from the user, our task is to run a search function/query inside the database.
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false
	})
} //the User is the model name. We use the .find() to search the user.
	//go to routes after this

//create a function that will allow us to register a new user.
module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,		
		lastName: params.lastName,
		email: params.email,
		mobileNo: bcrypt.hashSync(params.mobileNo, 8),
		password: bcrypt.hashSync(params.password, 10)
		//for hash salts, the lowest value is 8, highest is 31		
	})
	return user.save().then((user, err) => {
		return (err) ? false : true
	}) //lets create a promise that will determine the return upon success or failure in saving the new entry.
}
//create routes after this



//line below is to authenticate user
//dont forget to get the needed dependency from auth.js (line 4 above)
//this line is from auth.js
module.exports.login = (params) => {
	//the first thin that we are going to do is to check if the user exists base from our existing records.
	//we have two input fields in profile.html
		//1. email add and 2. password
	return User.findOne({ email: params.email }).then(user => {
		//upon getting the email parameter of the user, the entered data has to pass the following requirements.
		if(user === null) { return false }
			//if the email exists, continue the process by checking if the passwords match each other
		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)
		if(isPasswordMatched) {
			return { access: auth.createAccessToken(user.toObject()) }
		}else{
			return false;
		}
	})
}
// go to routes after this


//the lines below will allow us to display the users information upon logging in
module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined //line before is to block the password value before it even reaches the browser
		return user
	})
}
// go to routes after this

//dont forget to call course in line 5
//lets create a function that will allow us to add a specific course to the user
module.exports.enroll = (params) => {
	return User.findById(params.userId).then(user => {
		user.enrollments.push({ courseId: params.courseId })
		//save the changes inside the database.
		return user.save().then((user, err) => {
			//line below will allow us to insert the user inside the course collection to have a feature that will allow you to idenfity the students enrolled in a course.
			return Course.findById(params.courseId).then(course => {
				course.enrollees.push({ userId: params.userId })
				return course.save().then((course, err) => {
					return (err) ? false : true
				})
			})
		})
	})
}
//go to routes after this.