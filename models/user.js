const mongoose = require('mongoose');

//create a schema/blueprint for our users
const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First name is required.']
	},
	lastName: {
		type: String,
		required: [true, 'Last name is required.']
	},
	email: {
		type: String,
		required: [true, 'Email is required.']
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile number is required.']
	},
	password: {
		type: String,
		required: [true, 'Password is required.']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	//this will be an array of objects, a student can enroll to more than 1 subject
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, 'Course ID is required']
			}, //identifies the course/subject
			enrolledOn: {
				type: Date,
				default: new Date()
			}, //describes the time and day when the student enrolled for the subject
			status: {
				type: String,
				default: 'Enrolled' //alternative values can either be completed or cancelled
			} //describes if the student is accepted or rejected
		}
	]
});

// Now that we have defined the Schema, create a model
module.exports = mongoose.model('User', userSchema);
//go to controllers after this. make the user.js files