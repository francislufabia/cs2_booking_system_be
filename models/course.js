const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		uppercase: true,
		required: [true, 'Course name is required.']
	},
	description: {
		type: String,
		required: [true, 'Course description is required.']
	},
	price: {
		type: Number,
		required: [true, 'Course price is required.']
	},
	isActive: {
		type: Boolean,
		default: true
	}, //describes if the course is open for enrollment
	createdOn: {
		type: Date,
		default: new Date()
	}, //describes when the subject was added
	enrollees: [
		{ //pull the _id property from the mongoDB database
			userId: {	//this will describe the id of the user to get the details
				type: String,
				required: [true, "User ID is required."]
			},
			enrolledOn: { //when the student enrolled for the subject
				type: Date,
				default: new Date()
			}
		}
	]//displays the list of students that are enrolled for a course
});

module.exports = mongoose.model('Course', courseSchema);