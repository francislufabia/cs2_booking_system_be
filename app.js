let express = require ('express');
let mongoose = require('mongoose');
const app = express ();

require ('dotenv').config()
const port = process.env.PORT;
const connectionString = process.env.DB_CONNECTION_STRING;

const cors = require('cors');
app.use(cors()); //dont forget cors

mongoose.connect(connectionString,{useNewUrlParser: true, useUnifiedTopology: true})
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log("Connected to MongoDB Atlas Database"));

app.use(express.json()); //dont forget middleware
app.use(express.urlencoded({extended: true}))
//go make models after this

//from routes.js, add this
const userRoutes = require('./routes/user');
app.use('/api/users', userRoutes);
//this will add localhost:4000/api/users
//go to postman to test API after this. at the postman, the last part of the URL in postman (/register) is found in line 10 of the routes/user.js 

const courseRoutes = require('./routes/course');
app.use('/api/courses', courseRoutes);
//add 


//the lines below is to check if heroku is working
app.get('/',(req, res) => {
	res.send("successfully hosted in heroku")
})

app.listen(process.env.PORT, () => console.log(`Connected on port ${port}`));
//the process.env.PORT was added for heroku, before, the text was this:
// app.listen(port, () => console.log(`Connected on port ${port}`));
